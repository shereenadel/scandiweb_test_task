import {Link} from "react-router-dom";
import React, { useState } from 'react'
import insertProducts from "../api/insertProducts";

function AddProduct(){
    const [type, setType] = useState('')
    const [inputs, setInputs] = useState({})
    const [errors, setErrors]  = useState([])
    function handleTypeChange (val){
        setInputs(values => ({...values, ['type']: val}))
        setType(val)
    }
    function handleChange (event)  {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}))
    }
    function handleSubmit(event){
        insertProducts(event,inputs).then(function(response) {
            if(response.ok) {
                setErrors({})
                window.location.href = '/'
            }
            else{
                setErrors({});
                return response.json().then((json) => {
                    Object.entries(json).forEach(entry => {
                        const [key, value] = entry;
                        setErrors(values => ({...values, [key]: value}));
                })     
            })}})
        .catch((error) => {
            console.log(error);
     })      
    }
    return(
    <div className="container-fluid">
        <div className="Header">
            <h1> Product Add </h1>
            <button className="btn btn-primary Left-button" type="submit"  id="save" form="product_form" onClick={handleSubmit}>Save</button>
            <Link type="button" className="btn btn-secondary Right-button" to="/">Cancel</Link>
            <hr className="Hr"/> 
            <form id="product_form" method="post" className="Form">
                <div className="form-group">
                    <div className="Form-input">
                        <label className="col-md-2 Input-label">SKU</label>
                        <div className="col-md-8">
                            <input type="text" className="form-control Input-inline" name="sku" id="sku" onChange={handleChange}/>
                            {errors.sku && <p id="p_sku" className="Validation">{errors.sku}</p>}
                        </div>
                    </div>
                </div>
                    <div className="form-group">
                        <div className="Form-input">
                            <label className="col-md-2 Input-label" >Name</label>
                            <div className="col-md-8">
                                <input  type="text" className="form-control Input-inline" name="name"  id="name" onChange={handleChange}/>
                                {errors.name && <p  id="p_name" className="Validation">{errors.name}</p>}
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <div  className="Form-input">
                            <label className="col-md-2 Input-label">Price($)</label>
                            <div className="col-md-8">
                                <input  type="text" className="form-control Input-inline" name="price"  id="price" onChange={handleChange}/>
                                {errors.price && <p  id="p_price" className="Validation">{errors.price}</p>}
                            </div>
                        </div>
                    </div>
                    <br></br>
                    <div className="form-group" className="Form-input">
                        <label  className="col-md-4 Input-label" >Type Switcher</label>
                        <div className="col-md-6">
                            <select className="form-control form-control-sm" name="type" id="productType" onChange={(val) => handleTypeChange(val.target.value)} >
                                <option value="">Select Product Type</option>
                                <option value="Disk">DVD</option>
                                <option value="Furniture">Furniture</option>
                                <option value="Book">Book</option>
                            </select>
                            {errors.type && <p  id="p_type" className="Validation">{errors.type}</p>}
                        </div>
                    </div>
                    {
                        type == 'Disk' && 
                        <div id="Disk" className="product-Type">
                            <div className="form-group">
                                <div className="Form-input">
                                    <label className="col-md-2 Input-label" >Size(MB)</label>
                                    <div className="col-md-8">
                                        <input type="text" className="form-control Input-inline" name="size"  id="size" onChange={handleChange}/>
                                        {errors.size && <p  id="p_size" className="Validation">{errors.size}</p>}
                                    </div>
                                </div>
                            </div>
                            <p>Please provide size of product in MB</p>
                        </div>
                    }
                    {
                        type == 'Furniture' &&
                        <div id="Furniture" className="product-Type">
                            <div className="form-group">
                                <div className="Form-input">
                                    <label  className="col-md-2 Input-label">Height(CM)</label>
                                    <div className="col-md-8">
                                        <input type="text" className="form-control Input-inline" name="height"  id="height" onChange={handleChange}/>
                                        {errors.height && <p  id="p_height" className="Validation">{errors.height}</p>}
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="Form-input">
                                    <label  className="col-md-2 Input-label">Width(CM)</label>
                                    <div className="col-md-8">
                                        <input  type="text" className="form-control Input-inline" name="width"  id="width" onChange={handleChange}/>
                                        {errors.width && <p  id="p_width" className="Validation">{errors.width}</p>}
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="Form-input">
                                    <label  className="col-md-2 Input-label">Length(CM)</label>
                                    <div className="col-md-8">
                                        <input type="text" className="form-control Input-inline" name="f_length" id="f_length" onChange={handleChange}/>
                                        {errors.f_length && <p  id="p_f_length" className="Validation">{errors.f_length}</p>}
                                    </div>
                                </div>
                            </div>
                            <p>Please provide dimensions of product in HxWxL format</p>
                        </div>
                    }
                    {
                        type == 'Book' && 
                        <div id="Book" className="product-Type">
                            <div className="form-group">
                                <div className="Form-input">
                                    <label  className="col-md-2 Input-label">Weight(MB)</label>
                                    <div className="col-md-8">
                                        <input  type="text" className="form-control Input-inline" name="weight"  id="weight" onChange={handleChange}/>
                                        {errors.weight && <p  id="p_weight" className="Validation">{errors.weight}</p>} 
                                    </div>
                                </div>
                            </div>
                            <p>Please provide weight of the book</p>
                        </div>
                    }
            </form>
        </div>
    </div>
    );
}

export default AddProduct;