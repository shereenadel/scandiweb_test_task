import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from "react-router-dom";
import React, { useState, useEffect } from 'react'
import useGetProducts from "../api/useGetProducts";
import deleteProducts from "../api/deleteProducts";


import '../App.css';

function Home(){
    const [data] = useGetProducts()
    const [deletedItems, setDeletedItems] = useState([])
    function handleChange(target){
      console.log(deletedItems)
      if(target.value === true){
        const name = target.name;
        console.log(name)
        setDeletedItems(values =>  [...values, name])
      }
      else{
        setDeletedItems(values => values.filter(item => item !== target.name))
      }
    }
    return (
        <div className="App">
          <div className="container-fluid">
            <div className="Header">
              <div className='d-flex justify-content-between'>
                <h1> Product List </h1>
                <div className='d-flex'>
                  <Link type="button" className="btn btn-primary Left-button" to="/add-product" id="add">ADD</Link>
                  <button type="button" className="btn btn-danger Right-button" id="delete-product-btn" onClick={() => {
                    deleteProducts(deletedItems)
                    setDeletedItems([])
                  }}>MASS DELETE</button>
                </div>
              </div>
              <hr className='Hr'/> 
              <div className="d-flex flex-wrap" id="products">
              {data.length > 0 && data.map((product)=>
                <div className="col-sm-3" id={product.sku} key={product.sku}>
                  <div className="card Card-style">
                    <input className="delete-checkbox" type="checkbox" name={product.sku} onChange={(e) => 
                    {handleChange({
                          name: e.target.name,
                          value: e.target.checked,
                      });
                    }}/>
                    <h6 className="App">{product.sku}</h6>
                    <h6 className="App">{product.name}</h6>
                    <h6 className="App">{product.price + "$"}</h6> 
                    <h6 className="App">{product.property}</h6>   
                  </div>
                </div>)}
              </div>
            </div>
          </div>
        </div>
      );
}
export default Home;