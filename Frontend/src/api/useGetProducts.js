import { useState, useEffect } from 'react'

function useGetProducts(){
    const [data, setData] = useState({})
    useEffect(() => {
        fetch('http://localhost/Scandiweb/fetch-products.php', {
        method: 'GET',
        }).then((response) => response.json())
            .then((responseObject) => {
            console.log(responseObject);
            setData(responseObject)
            }
        )
        .catch((error) => {
            console.log('error');
        })
    }, [])
    return [data];
}

export default useGetProducts;