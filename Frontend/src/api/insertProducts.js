function insertProducts(event,inputs){
    event.preventDefault();
    const formData = new FormData();
    var errors = [];
    Object.entries(inputs).forEach(entry => {
        const [key, value] = entry;
        formData.append(key, value);
    })
    return fetch('http://localhost/Scandiweb/insert-product.php', {
            method: 'POST',
            body: new URLSearchParams(formData)
        })
}

export default insertProducts;