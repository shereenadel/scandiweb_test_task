function deleteProducts(deletedItems){
    const formData = new FormData();
    formData.append("products", deletedItems)
    fetch('http://localhost/Scandiweb/delete-product.php', {
      method: 'POST',
      body: new URLSearchParams(formData)
  }).then(function(response) {
      if(response.ok) {
          console.log(deletedItems)
        for(var i = 0; i < deletedItems.length; i++){
            console.log(deletedItems[i])
          var element = document.getElementById(deletedItems[i]);
          element.remove();
        }
      }})
  .catch((error) => {
    console.log(error);
  })
  }

  export default deleteProducts;