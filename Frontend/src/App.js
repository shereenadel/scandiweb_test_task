import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './pages/Home';
import AddProduct from './pages/AddProduct'; 


import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="add-product" element={<AddProduct />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
