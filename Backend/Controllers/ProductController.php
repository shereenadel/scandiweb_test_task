<?php

namespace Controllers;

use Config\DatabaseConnection;
use Validation\ProductValidate;

class ProductController
{
    public function delete($items)
    {
        try {
            $db = new DatabaseConnection();
            $connection = $db->getConnection();
            $products = explode(',',$items);
            foreach($products as $product){
                $query = 'DELETE FROM products WHERE products.sku="' . $product . '";';
                $connection->exec($query);
            }
            header('HTTP/1.1 200');
            echo 'Products deleted successfully';
        }
        catch(Exception $e) {
            header('HTTP/1.1 403');
            echo 'Error while deleting';
        }
    }
    public function fetchProducts()
    {
        $products = [];
        try {
            $db = new DatabaseConnection();
            $query = "SELECT products.sku, products.name, products.price, products.type, 
            GROUP_CONCAT(products_attributes.attribute, ': ', products_attributes.value) 
            as 'properties' FROM products  JOIN 
            products_attributes ON (products_attributes.product_id = products.id) 
            group by products.id;";
            $stmt = $db->getConnection()->prepare($query);
            $stmt->execute();
            while($row = $stmt->fetch()){
                $usedClass = 'Models\\'.$row['type'];
                $product = new $usedClass();
                $properties = [];
                foreach(explode(',', $row['properties']) as $property){
                    $values = explode(': ', $property);
                    if($values[0] == 'length'){
                        $values[0] = 'f_length';
                    }
                    $properties[$values[0]] = $values[1];
                }
                $product->setSku($row['sku']);
                $product->setName($row['name']);
                $product->setPrice($row['price']);
                $product->setProperty($properties);
                $product->setType($row['type']);
                $result = array(
                'sku' => $product->getSku(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'property' => $product->getPropertyString());
                array_push($products, $result);
            }
                header('HTTP/1.1 200');
                echo json_encode($products);
            }
        catch(Exception $e) 
        {
            header('HTTP/1.1 403');
            echo 'Error while fetching products';
        }
    }
    public function create($product)
    {
        $rules = array(
            'sku' => array('required', 'alphanumeric', 'unique'),
            'name' => array('required'),
            'price' => array('required', 'numeric'),
            'type' => array('required'),
            'weight' => array('required_if:type=Book', 'numeric'),
            'size' => array('required_if:type=Disk', 'numeric'),
            'f_length' => array('required_if:type=Furniture', 'numeric'),
            'width' => array('required_if:type=Furniture', 'numeric'),
            'height' => array('required_if:type=Furniture', 'numeric'),
        );
        $validation = new ProductValidate($product, $rules);
        $isValid = $validation->check();
        if(!$isValid){
            header('HTTP/1.1 422');
            echo json_encode($validation->getMessages());
            return;
        }
        try{
            $type = $product['type'];
            $usedClass = 'Models\\' . $type;
            $newProduct = new $usedClass;
            $newProduct->setSku($product['sku']);
            $newProduct->setName($product['name']);
            $newProduct->setPrice($product['price']);
            $newProduct->setType($product['type']);
            $newProduct->setProperty($product);
            $newProduct->create();
            header('HTTP/1.1 201');
            echo 'Product created successfully';
        }
        catch(Exception $e) {
            header('HTTP/1.1 403');
            echo 'Error while creating product';
        }
    }    
}