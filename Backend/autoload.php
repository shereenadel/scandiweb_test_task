<?php

spl_autoload_register(function($class)
{
        $ds = DIRECTORY_SEPARATOR;
        $dir = __DIR__;
        $className = str_replace('\\', $ds, $class);
        $file = "{$dir}{$ds}{$className}.php";
        require_once $file;
});