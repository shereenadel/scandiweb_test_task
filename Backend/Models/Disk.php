<?php

namespace Models;

use Config\DatabaseConnection;

class Disk extends Product
{
    private $size;

    public function setProperty($property)
    {  
        $this->size = $property['size'];
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    public function getProperty()
    {
        return array('size' => $this->size);
    }
    public function getPropertyString()
    {
        return 'Size: ' . $this->size . ' MB';
    }
    public function getType()
    {
        return $this->type;
    }
    private function getId()
    {
        $db = new DatabaseConnection();
        $query = $db->getConnection();
        $sku = $this->getSku();
        $sql ="select id from products where sku='$sku';";
        $result = $query->query($sql);
        $id = $result->fetch();
        return $id['id'];
    }
    public function create()
    {
        $db = new DatabaseConnection();
        $query = $db->getConnection();
        $sql = "INSERT INTO products(SKU, name, price, type) VALUES(?,?,?,?)";
        $stmt = $query->prepare($sql);
        $stmt->execute([$this->getSku(), $this->getName(), $this->getPrice(), $this->getType()]);
        $id = $this->getId();
        $sql = "INSERT INTO products_attributes(product_id, attribute, value) VALUES (?,?,?)";
        $stmt = $query->prepare($sql);
        $stmt->execute([$this->getId(), 'size', $this->size]);
    }
}

