<?php

namespace Models;

use Config\DatabaseConnection;

class Furniture extends Product{
    private $length;
    private $width;
    private $height;

    public function setProperty($properties)
    {
        $this->length = $properties['f_length'];
        $this->width = $properties['width'];
        $this->height = $properties['height'];
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    public function getproperty()
    {
        return array('length' => $this->length, 'width' => $this->width, 'height' => $this->height);
    }
    public function getPropertyString()
    {
        return 'Dimension: ' . $this->length . 'x' . $this->width . 'x' . $this->height;
    }
    public function getType()
    {
        return $this->type;
    }
    private function getId()
    {
        $db = new DatabaseConnection();
        $query = $db->getConnection();
        $sku = $this->getSku();
        $sql ="select id from products where sku='$sku';";
        $result = $query->query($sql);
        $id = $result->fetch();
        return $id['id'];
    }
    public function create()
    {
        $db = new DatabaseConnection();
        $query = $db->getConnection();
        $sql = "INSERT INTO products(SKU, name, price, type) VALUES(?,?,?,?)";
        $stmt = $query->prepare($sql);
        $stmt->execute([$this->getSku(), $this->getName(), $this->getPrice(), $this->getType()]);
        $id = $this->getId();
        $sql = "INSERT INTO products_attributes(product_id, attribute, value) VALUES (?,?,?)";
        $stmt = $query->prepare($sql);
        $stmt->execute([$this->getId(), 'length', $this->length]);
        $sql = "INSERT INTO products_attributes(product_id, attribute, value) VALUES (?,?,?)";
        $stmt = $query->prepare($sql);
        $stmt->execute([$this->getId(), 'width', $this->width]);
        $sql = "INSERT INTO products_attributes(product_id, attribute, value) VALUES (?,?,?)";
        $stmt = $query->prepare($sql);
        $stmt->execute([$this->getId(), 'height', $this->height]);
    }
}

