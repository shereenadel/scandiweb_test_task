<?php

namespace Models;

use Config\DatabaseConnection;

abstract class Product
{
    private $sku;
    private $name;
    private $price;
    protected $type;

    public function setSku($sku)
    {
        $this->sku = $sku;    
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    public function getSku()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    abstract function getPropertyString();
    abstract function setProperty($properties);
    abstract public function create();
    abstract public function setType($type);
    abstract public function getType();
}

