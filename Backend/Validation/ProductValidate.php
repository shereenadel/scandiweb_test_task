<?php

namespace Validation;

use Config\DatabaseConnection;

class ProductValidate
{
    private $rules;
    private $fields;
    private $messages = [];
    public function __construct($fields, $rules)
    {
        $this->rules = $rules;
        $this->fields = $fields;
    }
    public function check()
    {
        $isValid = true;
        foreach($this->rules as $attribute => $validations){
            $required = false;
            foreach($validations as $validation){
                if($validation == 'required' && empty($this->fields[$attribute])){
                    $isValid = false;
                    $this->messages[$attribute] = 'Please, submit required data';
                    $required = true;
                    break;
                }
                else if(($required || !empty($this->fields[$attribute])) && $validation == 'alphanumeric' 
                        && !ctype_alnum($this->fields[$attribute])){
                    $isValid = false;
                    $this->messages[$attribute] = 'Please, provide the data of indicated type';
                }
                else if(($required || !empty($this->fields[$attribute])) && $validation == 'numeric' 
                        && !is_numeric($this->fields[$attribute])){
                    $isValid = false;
                    $this->messages[$attribute] = 'Please, provide the data of indicated type';
                }
                else if(strpos($validation, 'required_if') !== false){
                    $type = explode('=', explode(':', $validation)[1]);
                    if(!empty($this->fields[$type[0]]) && $this->fields[$type[0]] == $type[1]){
                        $required = true;
                        if(empty($this->fields[$attribute])){
                            $isValid = false;
                            $this->messages[$attribute] = 'Please, submit required data';
                            break;
                        }
                    }
                }
                else if(($required || !empty($this->fields[$attribute])) && $validation == 'unique'){
                    $db = new DatabaseConnection();
                    $query = $db->getConnection();
                    $sql = "SELECT * FROM products WHERE sku=?";
                    $stmt = $query->prepare($sql);
                    $stmt->execute([$this->fields[$attribute]]);
                    $sku = $stmt->fetch();
                    if($sku){
                        $isValid = false;
                        $this->messages[$attribute] = 'SKU is not unique';
                        break;
                    }
                }
            }
        }
        return $isValid;
    }
    public function getMessages()
    {
        return $this->messages;
    }

}